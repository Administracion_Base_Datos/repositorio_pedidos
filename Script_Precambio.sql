--Informacion de la Base de Datos
--Importante verificar antes de realizar algun cambio
--V 1.0


--Base de datos
use Pedidos_Test
go

--Creacion de tablas temporales

/****** Object:  Table [catalogo].[ClienteCopia]    Script Date: 6/7/2019 14:06:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [catalogo].[ClienteCopia](
	[codcli] [char](3) NOT NULL,
	[codciu] [char](3) NOT NULL,
	[garante] [char](3) NOT NULL,
	[direnvio] [varchar](80) NULL,
	[credito] [numeric](7, 2) NULL,
	[descuento] [numeric](5, 2) NULL,
) ON [Secundario]
GO

/****** Object:  Table [movimiento].[CabezeraPCopia]    Script Date: 6/7/2019 14:06:46 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [movimiento].[CabezeraPCopia](
	[codped] [char](3) NOT NULL,
	[fecha] [datetime] NULL,
	[montototal] [numeric](10, 2) NULL,
	[codcli] [char](3) NOT NULL,
) ON [PRIMARY]
GO

/****** Object:  Table [movimiento].[DetallePCopia]    Script Date: 6/7/2019 14:06:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [movimiento].[DetallePCopia](
	[numlinea] [tinyint] NOT NULL,
	[preciou] [numeric](7, 2) NULL,
	[cantidad] [smallint] NULL,
	[codped] [char](3) NOT NULL,
	[codpro] [char](3) NOT NULL,
) ON [PRIMARY]
GO


-----------------------------------INSERTADO DE DATOS A LAS TABLAS TEMPORALES----------------------------------------------

--insertado de datos a las tablas temporales

USE Pedidos_Test
GO

INSERT INTO [CATALOGO].[CLIENTECOPIA] SELECT * FROM [CATALOGO].[CLIENTE]

INSERT INTO [MOVIMIENTO].[CABEZERAPCOPIA] SELECT * FROM [MOVIMIENTO].[CABEZERAP]

INSERT INTO [MOVIMIENTO].[DETALLEPCOPIA] SELECT * FROM [MOVIMIENTO].[DETALLEP]


------------------------------------TABLA CLIENTE -------------------------------------------------

exec sp_help 'catalogo.Cliente'

exec sp_depends 'catalogo.Cliente'

exec sp_helpindex 'catalogo.Cliente'

exec sp_helptrigger 'catalogo.Cliente'

exec sp_helpconstraint 'catalogo.Cliente'

exec sp_table_privileges 'catalogo.Cliente'

---------------------------REGISTRO CREACION PRIMARY KEY TABLA CLIENTE ------------------------------------

/****** Object:  Index [PK__Cliente__407E6F75E9D3FE40]    Script Date: 6/7/2019 15:39:41 ******/
ALTER TABLE [catalogo].[Cliente] ADD PRIMARY KEY CLUSTERED 
(
	[codcli] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [Secundario]
GO

USE [Pedidos_Test]
GO

SET ANSI_PADDING ON
GO

-------------------------------------REGISTRO CREACION INDICES CLIENTE -------------------------------

/****** Object:  Index [PK__Cliente__407E6F75E9D3FE40]    Script Date: 6/7/2019 16:18:23 ******/
ALTER TABLE [catalogo].[Cliente] ADD PRIMARY KEY CLUSTERED 
(
	[codcli] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [Secundario]
GO


------------------------------------------- TRIGGERS DE CLIENTE ----------------------------------------

select * from sys.triggers

-------------------------------------------- PROCEDURES CLIENTE ------------------------------------------

select * 
  from Pedidos_Test.information_schema.routines 
 where routine_type = 'PROCEDURE'


 -------------------------------------------VISTAS CLIENTE -----------------------------------------------
 select * 
  from Pedidos_Test.information_schema.tables
 where TABLE_TYPE = 'VIEW'


 ---------------------------- PEDIDOS (INCLUYE CABEZERAP Y DETALLEP) -------------------------------------------------

exec sp_help 'movimiento.CabezeraP'

exec sp_help 'movimiento.DetalleP'

exec sp_depends 'movimiento.CabezeraP'

exec sp_depends 'movimiento.DetalleP'

exec sp_helpindex 'movimiento.CabezeraP'

exec sp_helpindex 'movimiento.DetalleP'

exec sp_helptrigger 'movimiento.CabezeraP'

exec sp_helptrigger 'movimiento.DetalleP'

exec sp_helpconstraint 'movimiento.CabezeraP'

exec sp_helpconstraint 'movimiento.DetalleP'

exec sp_table_privileges 'movimiento.CabezeraP'

exec sp_table_privileges 'movimiento.DetalleP'


---------------------------REGISTRO CREACION PRIMARY KEY TABLA CABEZERAP ------------------------------------

SET ANSI_PADDING ON
GO

/****** Object:  Index [pk_CabezeraP]    Script Date: 6/7/2019 17:53:59 ******/
ALTER TABLE [movimiento].[CabezeraP] ADD  CONSTRAINT [pk_CabezeraP] PRIMARY KEY CLUSTERED 
(
	[codped] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

---------------------------REGISTRO CREACION INDICE TABLA CABEZERAP ------------------------------------


SET ANSI_PADDING ON
GO

/****** Object:  Index [pk_CabezeraP]    Script Date: 6/7/2019 17:59:35 ******/
ALTER TABLE [movimiento].[CabezeraP] ADD  CONSTRAINT [pk_CabezeraP] PRIMARY KEY CLUSTERED 
(
	[codped] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

-------DEBIDO A QUE LOS TRIGGERS, VISTAS Y PROCEDURES SE VEN DE TODA LA BASE DE DATOS, NO ES NECESARIO
-------VOLVER A CORRER ESOS COMANDOS.















